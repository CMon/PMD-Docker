FROM openjdk:21-slim-buster

ARG PMD_VERSION="7.9.0"

# install neede packages for installation
RUN apt-get update
RUN apt-get upgrade --yes
RUN DEBIAN_FRONTEND="noninteractive" apt-get install --yes --no-install-recommends \
    wget unzip


RUN wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F${PMD_VERSION}/pmd-dist-${PMD_VERSION}-bin.zip
RUN unzip pmd-dist-${PMD_VERSION}-bin.zip -d /opt
RUN rm pmd-dist-${PMD_VERSION}-bin.zip


ENV PATH="${PATH}:/opt/pmd-bin-${PMD_VERSION}/bin"
COPY files/bashrc /root/.bashrc

# cleanup
RUN DEBIAN_FRONTEND="noninteractive" apt-get remove --yes --purge wget unzip
RUN DEBIAN_FRONTEND="noninteractive" apt-get autoremove --yes
RUN rm -rf /var/lib/apt/lists/*
